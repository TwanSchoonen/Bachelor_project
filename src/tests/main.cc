// g++ --std=c++17 prog -lstdc++fs
#include <iostream>
#include <vector>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;
using namespace std;
 
int main()
{
	fs::recursive_directory_iterator iter("images");
	fs::recursive_directory_iterator end;
	while (iter != end)
	{
		// Add the name in vector
		listOfFiles.push_back(iter->path().string());

		error_code ec;
		// Increment the iterator to point to next entry in recursive iteration
		iter.increment(ec);
		if (ec) {
			std::cerr << "Error While Accessing : " << iter->path().string() << " :: " << ec.message() << '\n';
		}
	}
}
