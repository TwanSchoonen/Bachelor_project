#include "main.ih"

void haarcascadeDetect()
{
	VideoCapture cap;
	
	Arg &arg = Arg::instance();
	string media;

	if(arg.option(&media,'m'))
		cap.open(media);
	else
		cap.open(-1);

	Mat frame,image;

    // PreDefined trained XML classifiers with facial features
	CascadeClassifier face_cascade; 
	double scale=1;

	if (!face_cascade.load("haarcascades/haarcascade_frontalface_alt.xml"))
		cout << "error loading cascade\n";
	
    namedWindow(WINDOW_NAME, WINDOW_NORMAL);

	if (cap.isOpened())
	{
		// Capture frames from video and detect faces
		cout << "Face Detection Started...." << endl;
		while (true)
		{
			cap >> frame;
			if( frame.empty() )
				break;
			Mat frame1 = frame.clone();
			detectAndDraw(frame1, face_cascade, scale); 

			char c = (char)waitKey(10);
		
			// Press q to exit from window
			if( c == 27 || c == 'q' || c == 'Q' ) 
				break;
		}
	}
}
