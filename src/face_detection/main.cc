#include "main.ih"

namespace   // the anonymous namespace can be used here
{
	Arg::LongOption longOptions[] =
	{
		Arg::LongOption{"debug"},
		Arg::LongOption{"help", 'h'},
		Arg::LongOption{"version", 'v'},
		Arg::LongOption{"media", 'm'},
		Arg::LongOption{"algorithm", 'a'},
	};
	auto longEnd = longOptions + sizeof(longOptions) / sizeof(longOptions[0]);
}


int main(int argc, char **argv)
try
{
	Arg &arg = Arg::initialize("dhvm:a:",
							   longOptions, longEnd, argc, argv);

	// FaceDetect facedetect;
	string algorithm;

	if (arg.option(&algorithm,'a'))
	{
		if (algorithm.compare("haar") == 0)
			haarcascadeDetect();
		else if (algorithm.compare("dnn") == 0)
			deepnet();
		else
			cout << "algorithm: " << algorithm << ", not known, pleas choose a valid algorithm\n";
	}
	else
		haarcascadeDetect();

}
catch (int x)
{
    return Arg::instance().option("hv") ? 0 : x;
}
catch(std::exception const &exc)
{
    cerr << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "[Panic] Unknown exception thrown." << '\n';
    return 1;
}

