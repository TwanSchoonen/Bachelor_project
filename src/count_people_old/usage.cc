//                     usage.cc

#include "main.ih"

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << " [options]\n"
    "Where:\n"
    "   [options] - optional arguments (short options between parentheses):\n"
    "      --help (-h)      - provide this help\n"
    "      --version (-v)   - show version information and terminate\n"
    "      --debug (-d)     - show debug information\n"
    "      --media file     - examine the file for faces, if not specified standard input is used\n"
    "      --DNN            - Use the Deep Neural Net instead of the haar-cascade\n"
    "      --detect         - Only detect faces from input\n"
	"      --saveraw (-s)   - save all the detected faces and frames\n"
    "      --speed (-S)     - set waiting times between frames 0 means waiting for a click\n"
    "\n";
}
