#include "haarcasdetect.ih"

// This function is called every frame
void HaarCasDetect::detectFacesFromFrame(Mat &frame, vector<Rect> &faces)
{
    Mat frameGray;
	
    cvtColor(frame, frameGray, COLOR_BGR2GRAY);
    equalizeHist(frameGray, frameGray);

   d_cascade.detectMultiScale(frameGray, faces);
}
