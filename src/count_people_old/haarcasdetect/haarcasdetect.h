#ifndef INCLUDED_HAARCASDETECT_
#define INCLUDED_HAARCASDETECT_

#include "../options/options.h"
#include "../framedetector/framedetector.h"

class HaarCasDetect: public FrameDetector
{
	Options &d_options;

	cv::CascadeClassifier d_cascade;
	
public:

	HaarCasDetect();

	void detectFacesFromFrame(cv::Mat &frame, std::vector<cv::Rect> &faces);

private:

};

        
#endif
