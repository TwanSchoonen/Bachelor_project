#ifndef INCLUDED_FACECOMPARE_
#define INCLUDED_FACECOMPARE_

#include <opencv2/opencv.hpp>

#include <string>

class FaceCompare
{
	std::string d_dataPath = "database/";
	
public:
	FaceCompare();

	void neighborsFrameCompare();

	void displayImages();
	
	double compareHistograms(cv::Mat const &base, cv::Mat const &test, int method);

private:
	cv::Mat makeCanvas(std::vector<cv::Mat>& vecMat, int windowHeight, int nRows);
};
        
#endif
