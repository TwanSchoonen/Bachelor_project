#include "facecompare.ih"

void FaceCompare::displayImages()
{
	vector<Mat> images;
	
	fs::recursive_directory_iterator iter("database");
	fs::recursive_directory_iterator end;

	while (iter != end)
	{
		// Add the name in vector
		images.push_back(imread(iter->path().string()));

		error_code ec;
		// Increment the iterator to point to next entry in recursive iteration
		iter.increment(ec);
	}

	namedWindow("test", WINDOW_NORMAL);
	imshow("test", makeCanvas(images, 1000, 4));
	// waitKey(0);
	
}
