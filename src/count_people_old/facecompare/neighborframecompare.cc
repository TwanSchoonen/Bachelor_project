#include "facecompare.ih"

void FaceCompare::neighborsFrameCompare()
{
	Mat lastFrame, curFrame;
	int fileno = 0;
	double boundary = 0.6;

	lastFrame = imread("database/" + to_string(fileno) + ".jpg");
	
	while (true)
	{
		curFrame = imread("database/" + to_string(fileno + 1) + ".jpg");

		if (!lastFrame.data || !curFrame.data)
			break;

		// check if frames are the same
		if (compareHistograms(lastFrame, curFrame, 0) >= boundary)
		{
			cout << "removing fileno: " << fileno << ", because its the same as: " << fileno + 1 << "\n";
			fs::remove("database/" + to_string(fileno) + ".jpg");
		}

		lastFrame = curFrame;

		++fileno;
	}
}
