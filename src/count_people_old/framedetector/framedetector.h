#ifndef INCLUDED_FRAMEDETECTOR_
#define INCLUDED_FRAMEDETECTOR_

#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

class FrameDetector
{

	size_t d_faceWidth = 128;
	size_t d_faceHeight = 128;

	std::string d_directory = "database/";

	size_t d_filenumber = 0;
	
public:

	void saveFace(cv::Rect const &face, cv::Mat const &frame);

	virtual void detectFacesFromFrame(cv::Mat &frame, std::vector<cv::Rect> &faces) = 0;

	virtual ~FrameDetector();
   
private:

};
        
#endif
