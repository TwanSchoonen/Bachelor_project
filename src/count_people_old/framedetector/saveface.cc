#include "framedetector.ih"

void FrameDetector::saveFace(Rect const &face, Mat const &frame)
{
	// IMPORTANT: check if face is in the frame
	bool isInside = (face & cv::Rect(0, 0, frame.cols, frame.rows)) == face;

	// Only save whole faces
	if (isInside){

		Mat crop = frame(face);
		Mat res;
		resize(crop, res, Size(d_faceWidth, d_faceHeight), 0, 0, INTER_LINEAR); 

		Mat gray;
		cvtColor(crop, gray, CV_BGR2GRAY); 

		string filename = "database/";
		stringstream ssfn;
		ssfn << filename.c_str() << d_filenumber << ".jpg";
		filename = ssfn.str();

		bool succes = imwrite(filename, res); 
		if (succes)
			cout << "Saved: " << filename << "\n";
		else
			cout << "Error saving: " << filename << "\n";

		d_filenumber++;
	}
}
