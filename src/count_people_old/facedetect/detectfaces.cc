#include "facedetect.ih"

void FaceDetect::detectFaces()
{
	Mat frame;
	vector<Rect> faces;

	// Open a window where the progress will be shown
    namedWindow(d_options.windowName(), WINDOW_AUTOSIZE);

	if (d_cap.isOpened())
	{
		cout << "Face Detection Started...\n"
			"Press q to quit the window.\n";

		while (true)
		{
			d_cap >> frame;

			if(frame.empty())
				break;

			// Detect all faces form the current frame, and store them in faces
			d_frameDetector->detectFacesFromFrame(frame, faces);

			// Crop faces in new vector
			vector<Mat> cropedFaces = cropFaces(frame, faces);

			// Process all the detected faces
			if (!d_options.detectOnly())
				compareFaces(cropedFaces);

			showProcess(frame, faces);

			// If the -s flag is set we save all faces and frames
			if (d_options.saveRaw() /*&& cropedFaces.size()*/)
				saveRawInfo(frame, cropedFaces);

			int c = waitKey(d_options.mediaSpeed());

			// Press q to exit from window
			if( c == 27 || c == 'q' || c == 'Q' ) 
				break;
		}
	}
	cout << "Face Detection done!\n";
	if (d_options.saveRaw())
		 cout << "Saved: " << d_faceCount << " faces and " << d_frameCount << " frames\n";
}
