#include "facedetect.ih"

bool FaceDetect::compareWithKnownFaces(Mat const &face)
{
	double convidence; 

	for (size_t detectIdx = 0; detectIdx != d_detectedFaces.size(); ++detectIdx)
	{
		convidence = compareHistograms(d_detectedFaces[detectIdx],
											  face, d_histogramMethod);
		
		if (convidence >= d_confidenceThreshold)
		{
			d_detectedFaces[detectIdx] = face;
			return true;
		}
	}

	cout << "compare: " << convidence << "\n";
	return false;
}
