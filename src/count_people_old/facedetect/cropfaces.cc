#include "facedetect.ih"

vector<Mat> FaceDetect::cropFaces(Mat const &frame, std::vector<Rect> const &faces)
{
	vector<Mat> cropedFaces;
	// Is this tmp really needed
	Mat tmp;
	for (Rect const &face : faces)
	{
		// crop the frames, and resize the output to the desired result
		// we can add boundarys here
		resize(frame(face), tmp, Size(d_faceSize, d_faceSize));
		cropedFaces.push_back(tmp);
	}
	// Is the move constructor called?
	return cropedFaces;
}
