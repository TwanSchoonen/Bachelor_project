#include "facedetect.ih"

void FaceDetect::saveRawInfo(Mat const &frame, vector<Mat> const &faces)
{
	string frameFileName(d_framePath + to_string(d_frameCount) + ".jpg");
	if (!imwrite(frameFileName, frame))
		cerr << "Error saving: " << frameFileName << "\n";
	++d_frameCount;
	if (d_frameCount == 1000)
		d_framePath += "after/";

	for (Mat const &face : faces)
	{
		string faceFileName(d_facePath + to_string(d_faceCount) + ".jpg");
		if (!imwrite(faceFileName, face))
			cerr << "Error saving: " << faceFileName << "\n";
		++d_faceCount;
		if (d_faceCount == 1000)
			d_facePath += "after/";
	}
}
