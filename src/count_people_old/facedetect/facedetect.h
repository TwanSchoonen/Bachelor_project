#ifndef INCLUDED_FACEDETECT_
#define INCLUDED_FACEDETECT_

#include "../options/options.h"
#include "../framedetector/framedetector.h"

#include <opencv2/opencv.hpp>

class FaceDetect
{
	// Options set for this class
	Options &d_options;

	// The video stream
	cv::VideoCapture d_cap;

	// Pointer to the detection algorithm
	FrameDetector *d_frameDetector;

	std::vector<cv::Mat> d_detectedFaces;

	std::string d_facePath = "database/faces/";
	size_t d_faceCount;
	size_t d_faceSize = 128;

	std::string d_framePath = "database/frames/";
	size_t d_frameCount;

	// Vars for histogram comparison
	int d_histogramMethod = 0;
	double d_confidenceThreshold = 0.9;

public:

	FaceDetect();
	~FaceDetect();

	void detectFaces();

private:

	void openMedia();

	std::vector<cv::Mat> cropFaces(cv::Mat const &frame,
								   std::vector<cv::Rect> const &faces);

	void saveRawInfo(cv::Mat const &frame,
					 std::vector<cv::Mat> const &face);
	
	void compareFaces(std::vector<cv::Mat> const &faces);

	bool compareWithKnownFaces(cv::Mat const &face);

	void showProcess(cv::Mat &frame, std::vector<cv::Rect> &faces);

	double compareHistograms(cv::Mat const &base, cv::Mat const &test, int method);

	void cropFace(cv::Mat const &frame, cv::Rect const &area, cv::Mat &destination);

	cv::Mat makeCanvas(std::vector<cv::Mat>& vecMat, int windowHeight, int nRows);
};
        
#endif
