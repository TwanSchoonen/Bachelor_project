#include "facedetect.ih"

void FaceDetect::compareFaces(vector<Mat> const &faces)
{
	for (size_t faceIdx = 0; faceIdx != faces.size(); ++faceIdx)
	{
		if(!compareWithKnownFaces(faces[faceIdx]))
		{
			cout << "detected at new face at index " << d_detectedFaces.size() << "\n";
			d_detectedFaces.push_back(faces[faceIdx]);
		}
	}
}
