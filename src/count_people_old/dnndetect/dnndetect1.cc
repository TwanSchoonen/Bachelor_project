#include "dnndetect.ih"

DNNDetect::DNNDetect()
:
	d_options(Options::instance()),
	d_net(readNetFromCaffe("res/caffe/pyimage/deploy.prototxt.txt",
						   "res/caffe/pyimage/res10_300x300_ssd_iter_140000.caffemodel"))
{
	d_options.setDNNVars();
	cout << "Using DNN for detection\n"
		 << "With confidence = " << d_options.confidence() << "\n";
}
