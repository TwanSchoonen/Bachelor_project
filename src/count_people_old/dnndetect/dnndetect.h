#ifndef INCLUDED_DNNDETECT_
#define INCLUDED_DNNDETECT_

#include "../options/options.h"
#include "../framedetector/framedetector.h"

#include <opencv2/dnn.hpp>
#include <opencv2/opencv.hpp>

class DNNDetect: public FrameDetector
{
	Options &d_options;

	cv::dnn::Net d_net;

	size_t d_blobWidth = 300;
	size_t d_blobheight = 300;

public:

	DNNDetect();

	void detectFacesFromFrame(cv::Mat &frame, std::vector<cv::Rect> &faces);

private:
	
};
        
#endif
