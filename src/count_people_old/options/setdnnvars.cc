#include "options.ih"

void Options::setDNNVars()
{
	string tmp;
	if (d_arg.option(&tmp, "conf"))
		d_confidenceThreshold = stod(tmp);
	else
		d_confidenceThreshold = 0.7;

}
