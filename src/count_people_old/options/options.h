#ifndef INCLUDED_OPTIONS_
#define INCLUDED_OPTIONS_

#include <string>

namespace FBB
{
    class Arg;
}

// All available face detection algorithms
enum class FaceDetectAlgorithm
{
	HAAR,
	DNN
};

class Options
{
    FBB::Arg const &d_arg;

	FaceDetectAlgorithm d_fda;

	// true if the --media option is set
	bool d_mediaSet = false;
	// The to be read media
	std::string d_media;

	size_t d_mediaSpeed;

	// The name of the displayed window
	std::string const d_windowName = "counting people";

	bool d_detectOnly = false;
	bool d_saveRaw = false;
	bool d_clearDir = false;

    /* Haar cascade vars */

	std::string d_cascadePath;
	
    /* DNN vars */

	double d_confidenceThreshold;
	
	/*********************/


    static Options *s_options;

public:

	static Options &instance();

	Options(Options const &other) = delete;

	bool isMediaSet() const;
	std::string const &media() const;
	size_t mediaSpeed() const;
 
	std::string const &windowName() const;

	FaceDetectAlgorithm fda() const; 
	
	bool detectOnly() const;
	bool saveRaw() const;
	bool clearDir() const;

	void setFaceDetectVars();

	/* Haar cascade functions */
	
	void setHaarVars();

	std::string const &cascadePath() const;

	/* DNN functions */

	void setDNNVars();

	double confidence() const;
	
	/**************************/

private:

	Options();

};

inline bool Options::isMediaSet() const
{
	return d_mediaSet;
}

inline std::string const &Options::media() const
{
	return d_media;
}

inline size_t Options::mediaSpeed() const
{
	return d_mediaSpeed;
}

inline std::string const &Options::windowName() const
{
	return d_windowName;
}

inline FaceDetectAlgorithm Options::fda() const 
{
	return d_fda;
}

inline bool Options::detectOnly() const
{
	return d_detectOnly;
}

inline bool Options::saveRaw() const
{
	return d_saveRaw;
}

inline bool Options::clearDir() const
{
	return d_clearDir;
}

inline std::string const &Options::cascadePath() const
{
	return d_cascadePath;
}

inline double Options::confidence() const
{
	return d_confidenceThreshold;
}
#endif 
