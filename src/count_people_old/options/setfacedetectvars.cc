#include "options.ih"

void Options::setFaceDetectVars()
{
	if (d_arg.option(&d_media, "media"))
		d_mediaSet = true;

	// Haar cascade is the default
	d_fda = FaceDetectAlgorithm::HAAR;
	// If --DNN options is set we use the DNN
	if (d_arg.option(0, "DNN"))
		d_fda = FaceDetectAlgorithm::DNN;

	string tmp;
	if (d_arg.option(&tmp, 'S'))
		d_mediaSpeed = stoi(tmp);
	else //default mediaspeed
		d_mediaSpeed = 10;

	d_detectOnly = d_arg.option(0, "detect");

	d_saveRaw = d_arg.option('s');
	d_clearDir = d_arg.option('r');
}
