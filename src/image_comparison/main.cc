/**
 * @file compareHist_Demo.cpp
 * @brief Sample code to use the function compareHist
 * @author OpenCV team
 */

#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <fstream>


using namespace std;
using namespace cv;

const char* s1 = R"(\begin{table}[ht]
\centering
\caption{Histogram Comparison techniques}
\label{tab:histo_comp}
\resizebox{\textwidth}{!}{\begin{tabular}{lllllllll}
Method & \multicolumn{2}{l}{Correlation} & \multicolumn{2}{l}{Chi-Square} & \multicolumn{2}{l}{Intersection} & \multicolumn{2}{l}{Bhattacharyya distance} \\ \hline
Image  & same         & different        & same        & different        & same         & different         & same              & different              \\
)";

/**
 * @function main
 */
int main( int argc, char** argv )
{
    Mat src_base, hsv_base;
    Mat src_test1, hsv_test1;
    Mat src_test2, hsv_test2;

	size_t const tests = 20;

	ofstream file;
	file.open("/home/t/Git/Bachelor_project/thesis/tables/histogramcomp.tex");
	
	file << s1;

	for (size_t idx = 0; idx != tests; ++idx)
	{

		src_base = imread("images/" + to_string(idx) + "-1.jpg", 1);
		src_test1 = imread("images/" + to_string(idx) + "-2.jpg", 1);
		src_test2 = imread("images/" + to_string(idx) + "-3.jpg", 1);

		/// Convert to HSV
		cvtColor( src_base, hsv_base, COLOR_BGR2HSV );
		cvtColor( src_test1, hsv_test1, COLOR_BGR2HSV );
		cvtColor( src_test2, hsv_test2, COLOR_BGR2HSV );

		/// Using 50 bins for hue and 60 for saturation
		int h_bins = 50; int s_bins = 60;
		int histSize[] = { h_bins, s_bins };

		// hue varies from 0 to 179, saturation from 0 to 255
		float h_ranges[] = { 0, 180 };
		float s_ranges[] = { 0, 256 };

		const float* ranges[] = { h_ranges, s_ranges };

		// Use the o-th and 1-st channels
		int channels[] = { 0, 1 };


		/// Histograms
		MatND hist_base;
		MatND hist_test1;
		MatND hist_test2;

		/// Calculate the histograms for the HSV images
		calcHist( &hsv_base, 1, channels, Mat(), hist_base, 2, histSize, ranges, true, false );
		normalize( hist_base, hist_base, 0, 1, NORM_MINMAX, -1, Mat() );

		calcHist( &hsv_test1, 1, channels, Mat(), hist_test1, 2, histSize, ranges, true, false );
		normalize( hist_test1, hist_test1, 0, 1, NORM_MINMAX, -1, Mat() );

		calcHist( &hsv_test2, 1, channels, Mat(), hist_test2, 2, histSize, ranges, true, false );
		normalize( hist_test2, hist_test2, 0, 1, NORM_MINMAX, -1, Mat() );

		string out(to_string(idx + 1));
		/// Apply the histogram comparison methods
		for( int i = 0; i < 4; i++ )
		{
			int compare_method = i;
			// double base_base = compareHist( hist_base, hist_base, compare_method );
			double base_test1 = compareHist( hist_base, hist_test1, compare_method );
			double base_test2 = compareHist( hist_base, hist_test2, compare_method );

			out += " & " + to_string(base_test1) + " & " + to_string(base_test2);
		}

		file << out << "\t\\\\\n";
	}

	file << "\\end{tabular}}\n\\end{table}";

    cout << "Done \n";

    return 0;
}
