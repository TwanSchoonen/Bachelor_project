//                     usage.cc

#include "main.ih"

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << " [options]\n"
    "Where:\n"
    "   [options] - optional arguments (short options between parentheses):\n"
    "      --help (-h)      - provide this help\n"
    "      --version (-v)   - show version information and terminate\n"
    "      --debug (-d)     - show debug information\n"
    "      --media file     - examine the file for faces, if not specified standard input is used\n"
    "      --DNN            - Use the Deep Neural Net instead of the haar-cascade\n"
    "      --DNNconf        - specify the confidence for the DNN default = 0.7\n"
    "      --cascade file    - if using the haar cascade can be used to specify the path of the .xml file\n"
    "                         the default path: res/haarcascades/haarcascade_frontalface_alt.xml\n"
    "      --detect         - only detect faces from input\n"
	"      --saveraw (-s)   - save all the detected faces and frames in the database folder\n"
    "      --speed (-S)     - set waiting times between frames 0 means waiting for a click\n"
    "      --clear (-r)     - remove all files in the database folder\n"
    "      --hist           - specify the histogram method where 0 = Correlation, 1 = Chi-Square\n"
    "                         2 = Intersection and 3 = Bhattacharyya distance, the default is 0\n"
    "      --histconf       - specify the histogram confidence default is 0.5\n"
    "      --vectsize       - the amount of faces that is kept in memory to compare with default = 20\n"
    "\n";
}
