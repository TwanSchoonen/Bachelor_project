#include "facedetect.ih"

void FaceDetect::saveRawInfo(Mat const &frame, vector<Mat> const &faces)
{
	string frameFileName(d_framePath + to_string(d_frameCount - 1) + ".jpg");
	if (!imwrite(frameFileName, frame))
		cerr << "Error saving: " << frameFileName << "\n";

	if (d_frameCount == 1000)
		d_framePath += "after/";

	for (size_t idx = faces.size(); idx != 0; --idx)
	{
		string faceFileName(d_rawFacePath + to_string(d_rawFaceCount - 1 - idx) + ".jpg");
		if (!imwrite(faceFileName, faces[faces.size() - idx]))
			cerr << "Error saving: " << faceFileName << "\n";
	}
}
