#include "facedetect.ih"

void FaceDetect::replaceElement(FaceObject const &faceObj)
{
	size_t minIdx = 0;
	for (size_t idx = 1; idx != d_detectedFaces.size(); ++idx)
		if (d_detectedFaces[idx].lastFrameModified() <
			d_detectedFaces[minIdx].lastFrameModified())
			minIdx = idx;
	if (d_options.verbose())
		cout << "Last modified face at index: " << minIdx << ", savind and updating\n";
 	
	saveFace(d_detectedFaces[minIdx]);
	d_detectedFaces[minIdx] = faceObj;
}
