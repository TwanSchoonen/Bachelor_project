#include "facedetect.ih"

void FaceDetect::compareFaces(vector<Mat> const &faces)
{
 
	for (size_t idx = 0; idx != faces.size(); ++idx)
	{
		FaceObject faceObj(faces[idx], d_frameCount);
		if (!sameAsKnownFaces(faceObj))
		{
			if (d_detectedFaces.size() < d_options.vectSize())
				d_detectedFaces.push_back(faceObj);
			else
				replaceElement(faceObj);
		}
	}
}
