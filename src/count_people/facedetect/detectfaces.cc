#include "facedetect.ih"

void FaceDetect::detectFaces()
{
	Mat frame;
	vector<Rect> faces;
	int c = 0;

	// Open a window where the progress will be shown
    namedWindow(d_options.windowName(), WINDOW_AUTOSIZE);

	if (d_cap.isOpened())
	{
		cout << "Face Detection Started...\n"
			"Press q to quit the window.\n";

		while (true)
		{
			d_cap >> frame;

			if(frame.empty())
				break;

			// Detect all faces form the current frame, and store them in faces
			d_frameDetector->detectFacesFromFrame(frame, faces);

			// Crop faces in new vector
			vector<Mat> cropedFaces;
			cropedFaces.reserve(faces.size());
			cropFaces(frame, faces, cropedFaces);

			// Process all the detected faces
			if (!d_options.detectOnly())
				compareFaces(cropedFaces);

			showProcess(frame, faces, c == 's');

			incrementCounters(faces.size());

			if (d_options.saveRaw())
				saveRawInfo(frame, cropedFaces);

			c = waitKey(d_options.mediaSpeed());

			// Press q to exit from window
			if( c == 27 || c == 'q' || c == 'Q' ) 
				break;
		}
	}

	cout << "Face Detection done!\n";

	postProcess();
}
