#include "facedetect.ih"

void FaceDetect::saveFace(FaceObject const &faceObj) 
{
	string faceFileName(d_comparedFacePath + to_string(d_comparedFaceCount) + ".jpg");
	if (!imwrite(faceFileName, faceObj.image()))
		cerr << "Error saving: " << faceFileName << "\n";
	++d_comparedFaceCount;
}
