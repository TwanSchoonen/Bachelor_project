#include "facedetect.ih"

bool FaceDetect::compareHistograms(MatND const &hist_base, MatND const &hist_test) const
{
	double confidence = compareHist(hist_base, hist_test, d_options.histMethod());
	if (d_options.verbose())
		cout << "hist conf = " << confidence << "\n";

	if (d_options.histMethod() & 1)
		return confidence < d_options.histConfidence();
	return confidence >= d_options.histConfidence();
}
