#include "facedetect.ih"

void FaceDetect::postProcess()
{
	// Save all the faces still in memory
	for (size_t idx = 0; idx != d_detectedFaces.size(); idx++)
		saveFace(d_detectedFaces[idx]);

	cout << "Found: " << d_rawFaceCount << " faces and saved: "
		 << d_comparedFaceCount <<  " distinct faces\n";
}
