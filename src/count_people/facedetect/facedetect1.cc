#include "facedetect.ih"

FaceDetect::FaceDetect()
:
    d_options(Options::instance()),
	d_comparedFaceCount(0),
	d_rawFaceCount(0),
	d_frameCount(0)
{
	d_options.setFaceDetectVars();

	d_options.fda() == FaceDetectAlgorithm::DNN ?
		d_frameDetector = new DNNDetect :
		d_frameDetector = new HaarCasDetect;

	if (d_options.clearDir())
		fs::remove_all("database/");

	fs::create_directories("database/out/");
	fs::create_directories("database/faces/after/");
	fs::create_directories("database/frames/after/");

	openMedia();

	if (!d_options.detectOnly())
	{
		cout << "Using histogram method: " << d_options.histMethod()
			 << ", with conf = " << d_options.histConfidence() << "\n";
		d_detectedFaces.reserve(d_options.vectSize());
	}
}
