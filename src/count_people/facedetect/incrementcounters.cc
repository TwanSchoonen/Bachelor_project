#include "facedetect.ih"

void FaceDetect::incrementCounters(size_t faces)
{
	++d_frameCount;

	d_rawFaceCount += faces;
	if (d_options.verbose())
		cout << "Found: " << faces << " faces in the frame " << d_frameCount
			 << ", and: " <<  d_rawFaceCount << " faces in total\n";
}
