#ifndef INCLUDED_FACEDETECT_
#define INCLUDED_FACEDETECT_

#include "../options/options.h"
#include "../framedetector/framedetector.h"
#include "../faceobject/faceobject.h"

#include <opencv2/opencv.hpp>

class FaceDetect
{
	// Options set for this class
	Options &d_options;

	// The video stream
	cv::VideoCapture d_cap;

	// Pointer to the detection algorithm
	FrameDetector *d_frameDetector;

	// vector with the detected faces
	std::vector<FaceObject> d_detectedFaces;

	// Size of the images
	size_t d_faceSize = 128;

	std::string d_comparedFacePath = "database/out/";
	size_t d_comparedFaceCount;

	std::string d_rawFacePath = "database/faces/";
	size_t d_rawFaceCount;

	std::string d_framePath = "database/frames/";
	size_t d_frameCount;

public:

	FaceDetect();
	~FaceDetect();

	void detectFaces();

private:

	void openMedia();

	void cropFaces(cv::Mat const &frame, std::vector<cv::Rect> const &faces,
				   std::vector<cv::Mat> &dest) const;

	void compareFaces(std::vector<cv::Mat> const &faces);
	bool sameAsKnownFaces(FaceObject const &faceObj);
	void replaceElement(FaceObject const &faceObj);
	bool compareHistograms(cv::MatND const &base, cv::MatND const &test) const;

	void incrementCounters(size_t faces);

	void saveRawInfo(cv::Mat const &frame, std::vector<cv::Mat> const &face);
	
	void showProcess(cv::Mat &frame, std::vector<cv::Rect> &faces, bool save) const;

	cv::Mat makeCanvas(std::vector<FaceObject> const &vecMat,
					   int windowHeight, int nRows) const;

	void postProcess();

	void saveFace(FaceObject const &faceObj);
};
        
#endif
