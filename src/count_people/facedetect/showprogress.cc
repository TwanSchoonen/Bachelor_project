#include "facedetect.ih"

void FaceDetect::showProcess(cv::Mat &frame, std::vector<cv::Rect> &faces, bool save) const
{
	// draw rectangles over the faces
    for (size_t idx = 0; idx < faces.size(); ++idx) 
		rectangle(frame, faces[idx], Scalar(0, 255, 0));

	// TODO check if the frame is too large

	Mat output;
	Mat detectedImages;

	if (d_detectedFaces.size() == 0 || d_detectedFaces.size() > 50)
		detectedImages = Mat::zeros(Size(1,1), frame.type());
	else
	{
		int rows = d_detectedFaces.size() / 6 + 1;
		detectedImages = makeCanvas(d_detectedFaces, 300, rows);
	}

	resize(detectedImages, detectedImages, frame.size());

	vconcat(frame, detectedImages, output);
	
	imshow(d_options.windowName(), output);

	if (save)
		imwrite("./database/" + to_string(d_frameCount) + ".jpg", output);

}
