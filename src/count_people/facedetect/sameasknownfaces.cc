#include "facedetect.ih"

bool FaceDetect::sameAsKnownFaces(FaceObject const &faceObj)
{
	for (size_t idx = 0; idx != d_detectedFaces.size(); ++idx)
	{
		if (compareHistograms(d_detectedFaces[idx].histogram(), faceObj.histogram()))
		{
			d_detectedFaces[idx] = faceObj;
			return true;
		}
	}
	return false;
}
