#include "facedetect.ih"

void FaceDetect::openMedia()
{
	if (d_options.isMediaSet())
	{
		if (!d_cap.open(d_options.media()))
			return;
		cout << "detect and save with media: " << d_options.media() << "\n";
	}
	else // If no media is selected go for default
		d_cap.open(-1);
}
