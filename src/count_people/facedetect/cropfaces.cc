#include "facedetect.ih"

void FaceDetect::cropFaces(Mat const &frame, vector<Rect> const &faces, vector<Mat> &dest) const
{
	// increment the total face count
	for (size_t idx = 0; idx != faces.size(); ++idx)
	{
		Mat face;
		// crop the frames, and resize the output to the desired result
		resize(frame(faces[idx]), face, Size(d_faceSize, d_faceSize));
		dest.push_back(face);
	}
}
