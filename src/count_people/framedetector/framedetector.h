#ifndef INCLUDED_FRAMEDETECTOR_
#define INCLUDED_FRAMEDETECTOR_

#include <opencv2/opencv.hpp>
#include <string>
#include <vector>

class FrameDetector
{
	
public:

	virtual void detectFacesFromFrame(cv::Mat &frame, std::vector<cv::Rect> &faces) = 0;

	virtual ~FrameDetector();
   
private:

};
        
#endif
