#ifndef INCLUDED_FACEOBJECT_
#define INCLUDED_FACEOBJECT_

#include <opencv2/opencv.hpp>

class FaceObject
{
	cv::Mat d_image;
	cv::MatND d_histogram;
	size_t d_lastFrameModified;
	
public:
	FaceObject(cv::Mat const &image, size_t lastFrameModified);

	cv::Mat image() const;
	cv::MatND histogram() const;
	size_t lastFrameModified() const;
	//override operator= for faster assignments?
	
private:
	// Static?
	void computeHistogram();
};

inline cv::MatND FaceObject::image() const
{
	return d_image;
}
        
inline cv::MatND FaceObject::histogram() const
{
	return d_histogram;
}

inline size_t FaceObject::lastFrameModified() const
{
	return d_lastFrameModified;
}
#endif
