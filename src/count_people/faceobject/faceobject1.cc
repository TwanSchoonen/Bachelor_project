#include "faceobject.ih"

FaceObject::FaceObject(Mat const &image, size_t lastFrameModified)
:
	d_image(image),
	d_lastFrameModified(lastFrameModified)
{
	computeHistogram();
}
