#include "faceobject.ih"

void FaceObject::computeHistogram()
{
	Mat tmp_hsv;
	cvtColor(d_image, tmp_hsv, COLOR_BGR2HSV);

	// Using 50 bins for hue and 60 for saturation
    int h_bins = 50; int s_bins = 60;
    int histSize[] = { h_bins, s_bins };

	// hue varies from 0 to 179, saturation from 0 to 255
	float h_ranges[] = { 0, 180 };
	float s_ranges[] = { 0, 256 };

	const float* ranges[] = { h_ranges, s_ranges };

	const int channels[] = {0, 1};

	calcHist( &tmp_hsv, 1, channels, Mat(), d_histogram, 2, histSize, ranges);
	normalize( d_histogram, d_histogram, 0, 1, NORM_MINMAX, -1, Mat());
}
