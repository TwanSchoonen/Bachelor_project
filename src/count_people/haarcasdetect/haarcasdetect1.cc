#include "haarcasdetect.ih"

HaarCasDetect::HaarCasDetect()
:
	d_options(Options::instance())
{
	d_options.setHaarVars();

	cout << "Using Haar-cascade for detection\n"
		 << "With as cascade: " << d_options.cascadePath() << "\n";

	if (!d_cascade.load(d_options.cascadePath()))
		throw std::invalid_argument("wrong cascade file");

}
