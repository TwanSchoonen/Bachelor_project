#include "main.ih"

namespace
{
    Arg::LongOption longOptions[] =
    {
        Arg::LongOption{"help",                        'h'},
        Arg::LongOption{"usage",                       'h'},
        Arg::LongOption{"version",                     'v'},
        Arg::LongOption{"debug",                       'd'},
        Arg::LongOption{"saveraw",                     's'},
        Arg::LongOption{"speed",                       'S'},
        Arg::LongOption{"verbose",                     'V'},
        Arg::LongOption{"clear",                       'r'},
        Arg::LongOption{"media",                       Arg::Required},
        Arg::LongOption{"DNN",                         Arg::None},
        Arg::LongOption{"DNNconf",                     Arg::Required},
        Arg::LongOption{"detect",                      Arg::None},
        Arg::LongOption{"cascade",                     Arg::Required},
        Arg::LongOption{"hist",                        Arg::Required},
        Arg::LongOption{"histconf",                    Arg::Required},
        Arg::LongOption{"vectsize",                    Arg::Required},
    };

    auto longEnd = longOptions +
		sizeof(longOptions) / sizeof(Arg::LongOption);
}

// Main is mostly responsible for parsing command line arguments
// It uses the ARG class of the bobcat library to do so
int main(int argc, char **argv)
try
{
	// Specifies what arguments are allowed
    Arg &arg = Arg::initialize("dhvsVS:r",
                    longOptions, longEnd, argc, argv);

	// Makes sure help is used and 0 arguments are neccessary
    arg.versionHelp(usage, Icmbuild::version, 0);

	FaceDetect faceDetect;

	faceDetect.detectFaces();

}
catch (int x)
{
    return Arg::instance().option("hv") ? 0 : x;
}
catch(std::exception const &exc)
{
    cerr << exc.what() << '\n';
    return 1;
}
catch (...)
{
    cerr << "[Panic] Unknown exception thrown." << '\n';
    return 1;
}
