#include "options.ih"

void Options::setCompareVars()
{
	string tmp;
	d_histMethod = d_arg.option(&tmp, "hist") ?
		stoi(tmp) :
		0; // Default method

	d_histConfidence = d_arg.option(&tmp, "histconf") ?
		stod(tmp) :
		0.5; // Default confidence

	d_vectSize = d_arg.option(&tmp, "vectsize") ?
		stoi(tmp) :
		20; // Default vect size
}
