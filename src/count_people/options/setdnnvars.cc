#include "options.ih"

void Options::setDNNVars()
{
	string tmp;
	if (d_arg.option(&tmp, "DNNconf"))
		d_dnnConfidence = stod(tmp);
	else
		d_dnnConfidence = 0.7;

}
