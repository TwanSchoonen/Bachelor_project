#include "options.ih"

void Options::setFaceDetectVars()
{
	// check if --media is set
	// boolean is needed so we open default
	// camera if not, since that is done with int
	d_mediaSet = d_arg.option(&d_media, "media");
	
	// Haar cascade is the default
	// If --DNN options is set we use the DNN
	d_fda = d_arg.option(0, "DNN") ?
		FaceDetectAlgorithm::DNN :
		FaceDetectAlgorithm::HAAR;

	string tmp;
	d_mediaSpeed = d_arg.option(&tmp, 'S') ?
		stoi(tmp) :
		10; // Default speed

	d_saveRaw = d_arg.option('s');
	d_clearDir = d_arg.option('r');
	
	d_detectOnly = d_arg.option(0, "detect");
	if (!d_detectOnly)
		setCompareVars();
}
