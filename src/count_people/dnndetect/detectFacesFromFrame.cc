#include "dnndetect.ih"

void DNNDetect::detectFacesFromFrame(Mat &frame, vector<Rect> &faces)
{
	Mat inputBlob = blobFromImage(frame, 1, Size(300, 300),
							  Scalar(104, 117, 123), false, false);

    //set the network input
	d_net.setInput(inputBlob, "data"); 

    //compute output
	Mat detection = d_net.forward("detection_out"); 

	Mat detectionMat(detection.size[2], detection.size[3],
					 CV_32F, detection.ptr<float>());

	faces.clear();
	
	for(int i = 0; i < detectionMat.rows; i++)
	{
		float confidence = detectionMat.at<float>(i, 2);

		if(confidence > d_options.dnnConfidence())
		{
			int xLeftBottom = static_cast<int>(detectionMat.at<float>(i, 3) * frame.cols);
			int yLeftBottom = static_cast<int>(detectionMat.at<float>(i, 4) * frame.rows);
			int xRightTop = static_cast<int>(detectionMat.at<float>(i, 5) * frame.cols);
			int yRightTop = static_cast<int>(detectionMat.at<float>(i, 6) * frame.rows);

			Rect face(Rect(xLeftBottom, yLeftBottom,
						   xRightTop - xLeftBottom,
						   yRightTop - yLeftBottom));
			
			if ((face & cv::Rect(0, 0, frame.cols, frame.rows)) == face)
				faces.push_back(face);
		}
	}



}
