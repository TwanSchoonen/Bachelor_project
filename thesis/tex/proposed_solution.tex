\chapter{Proposed Solution}\label{sec:proposed_solution}

In the relevant work we discussed multiple methods on multiple topics.
Given the material reviewed in Chapter 2, we have seen that no ready-to-use method exist for our specific task. Hence, we propose here a pipeline that aims to solve this problem. We next describe the different steps of the pipeline, as follows.\\
Section~\ref{sec:detect_faces} describes the methods for detecting faces.\\
Section~\ref{sec:compute_and_compare_histograms} explains the process of computing and comparing the histograms.\\
Section~\ref{sec:overview} puts the more detailed descriptions together and shows an overview.\\

The general pipeline is seen in figure~\ref{fig:proposed_solution_pipeline}.
In this image we also the stages without relevant sections.
These stages are mentioned in the overview.
In this figure we also note the data types that we use.
\input{./figures/proposed_solution_pipeline}

\section{Detect Faces}\label{sec:detect_faces}

As stated in the conclusion of the relevant work, we use two different face detection algorithms and test their performance.
After evaluation of the obtained results we will pick the best one to use in out final product.
In the software we add an option to select the desired method.
We assume in this step that we have successfully loaded a video source already.
The flowchart for the whole face detection is shown in~\ref{fig:proposed_solution_detect}.

\input{./figures/proposed_solution_detect}

More detail about how dnnDetect and haarDetect work, is given in the upcoming subsections.

\subsection{Haar Cascade}

For the Haar-cascade~\cite{viola2004robust}, we use multiple trained cascade files provided with the opencv library~\cite{opencv}.
Not all these files are meant for face detection, we do only compare the files that are trained for face detection.
The provided xml files for face detection are:
\begin{itemize}
\item haarcascade\_frontalface\_alt2.xml
\item haarcascade\_frontalface\_alt\_tree.xml  
\item haarcascade\_frontalface\_alt.xml
\item haarcascade\_frontalface\_default.xml   
\end{itemize}
In our results we remove the haarcascade\_frontalface\_ part for a less wide output table.
If we do not choose a file as trained cascade the default file is used, being alt2.xml.
We load one of these files and then call the function: 
\begin{verbatim}
  void cv::CascadeClassifier::detectMultiScale(
    InputArray 	image,
    std::vector<Rect> &objects,
    double 	scaleFactor = 1.1,
    int 	minNeighbors = 3,
    int 	flags = 0,
    Size 	minSize = Size(),
    Size 	maxSize = Size() 
  )	
\end{verbatim}
Here we see that there are 5 parameters with default values.
For our experiments we choose to keep all the parameters at their default values.
Before feeding the frame to this function we first convert the frame to gray-scale. 

\subsection{Deep Neural Net}

For the deep neural net~\cite{farfade2015multi} we use one trained model and one specification file.
The trained model has the file extension ``.caffemodel''.
Here we have no option to set any one of these files, and thus we always use the default ones.
These files are: ``deploy.prototxt.txt'' and ``res10\_300x300\_ssd\_iter\_140000.caffemodel''.
An option that is able to change is the confidence level.
Since we use a neural net we get a list of faces (rectangles) and there confidence.
We only accept a face if the confidence is higher than the specified confidence.
The function that we use to get the output layers is:
\begin{verbatim}
Mat cv::dnn::blobFromImage	(InputArray image,
    double 	scalefactor = 1.0,
    const Size & 	size = Size(),
    const Scalar & 	mean = Scalar(),
    bool 	swapRB = true,
    bool 	crop = true 
)	
\end{verbatim}
Here we keep the scalefactor at the default.
However we set the Size to the trained size being (300, 300).
Also we set the mean to (104, 117, 123). And both swapRB and crop are set to false.
This is done because it gave us the best results and the source for our trained model suggested it.

\section{Compute And Compare Histograms}\label{sec:compute_and_compare_histograms}

We use histogram comparison because it works on all kinds of images that we have and our main purpose here is to reduce the amount of repeated images.
Computing and comparing the histograms of the facial images has multiple steps.
First for all images that are found in a frame, we compare them with all faces in the detectedfaces vector.
This vector contains all the distinct faces so far. Alse this vector has variable size and different values for this size will be tested.
What this size means, the amount of faces in memory where we compare with.
So lets say we use a size of 20 then each new face will be compared with the last 20 distinct faces.
If we find a new distinct face we will save the last modified face and update it to the new face.
With last modified we mean the longest not updated face.
We update a face when a face is found the same as one in the vector.
The comparison of images is done with histograms.\\

To compute the histogram of an image we apply the following steps:
\begin{enumerate}
\item Convert the colors to hsv
\item Specify the number of bins
\item Specify the color ranges
\item Compute the histogram
\item Normalize the histogram
\end{enumerate}
Once this is done we also save these histograms for later comparison.\\

To compare two histograms ( $H_{1}$ and $H_{2}$ ), first we have to choose a metric ($d(H_{1}, H_{2})$) to express how well both histograms match.
OpenCV~\cite{opencv} implements the function compareHist to perform a comparison. It also offers 4 different metrics to compute the matching.
For all these metrics we have to set some settings being:
\begin{itemize}
\item The bins for hue and saturation, we pick 50 and 60
\item The ranges for hue and saturation, we pick $[0-180)$ and $[0-256)$
\item The channels, we pick $[0,1]$
\end{itemize}
We will list in the upcoming subsections the different histogram comparisons that we will compare.


\subsection{Correlation}

The first method as metric is the correlation.  The distance is given by the following formula:
$$ d(H_{1}, H_{2}) = \frac{\sum_I(H_1(I) - \overline{H}_1)(H_2(I) - \overline{H}_2)}{\sqrt{\sum_I(H_1(I) - \overline{H}_1)^2\sum_I(H_2(I) - \overline{H}_2)^2}}$$
where
$$ \overline{H}_k = \frac{1}{N}\sum_JH_k(J) $$
and N is the total number of histogram bins.

\subsection{Chi-Square}

The second metric is the Chi-Square it is given by the following formula:
$$ d(H_{1}, H_{2}) = \sum_I\frac{(H_1(I) - H_2(I))^2}{H_1(I)}$$

\subsection{Intersection}

The formula below defines the third metric:
$$ d(H_{1}, H_{2}) = \sum_Imin(H_1(I), H_2(I))$$

\subsection{Bhattacharyya distance}

As last the Bhattacharyya distance metric formulate is presented below:
$$ d(H_{1}, H_{2}) = \sqrt{1-\frac{1}{\sqrt{\overline{H}_1 \overline{H}_2 N^2}}}\sum\sqrt{H_1(I) \cdot H_2(I)} $$

\section{Overview}\label{sec:overview}

Here we present a more detailed overview of the pipeline of our solution.
We start with the uncovered small steps from the large pipeline, being:
\begin{itemize}
\item Open media, the process of opening the media stream. This is done with the \texttt{-{}-media} flag. If this flag is not set we open the default media stream, this most of the time is your webcam.
\item Crop faces, once we got a frame and a vector of rectangle from the face detection method. We make a new image corresponding to the returned rectangle. Then we resize this rectangle to a square with variable size. This size is not changeable and for the rest of the project we use a size of 128.
\item Show progress, the video with the detected faces and the distinct vector is shown to the user. The output example will be shown in the results section.
\item Save distinct faces, here we can also have to option to save all faces and all frames. This is done for obtaining the results that we will present in the results section. If this option is set all the frames will be saved to: \texttt{database/frames/} and all the faces will be saved to: \texttt{database/faces/}. However if the option is not set these folders remain empty. The final distinct faces are saved in: \texttt{database/out}. 
\end{itemize}

Below we put all the smaller section together to get an image of the proposed solution as a whole.

\input{./figures/proposed_solution_overview}

