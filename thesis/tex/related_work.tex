\chapter{Related Work}\label{sec:related_work}

In order to find the best methods for counting people in videos, a literature study is performed.
An ideal result for this project will be a software system able to:
\begin{itemize}
\item Read a video and detect and crop all the faces present in a frontal position.
\item Count how many people are in the video as accurate as possible.
\item Group faces based on similarity; for each group, select the best ``representative'' face based on quality criteria including resolution, sharpness, contrast, and lighting.
\end{itemize}
To obtain this ideal result multiple papers are discussed, on multiple topics.
These papers are about our four steps of recognizing people in videos.
For each of these four steps a more detailed description is given in the relevant section.
In these sections we talk about relevant papers that fall in the discussed topic.
At the end of this chapter a summary is given of some methods that we discuss and a small conclusion on our findings is provided.\\
The upcoming sections are:
\begin{enumerate}
\item Face Detection~\ref{sec:face_detection}, detecting the faces in each frame. After that the faces are stored in a database or in memory.
\item Image Comparison~\ref{sec:image_comparison}, since we save the faces for each frame we need some method to distinguish the frames that contain the same person, here image comparison comes to help.
\item Face Clustering~\ref{sec:face_clustering}, this process clusters different persons for example on race, gender, and age range. This then can be used for to ask complicated questions to the database. However we face the risk of losing identities by grouping them.
\item Face Recognition~\ref{sec:face_recognition}, this is the process of actually giving a label to each face, so instead of saying this is a face, we are able to say this face belongs to person X. 
\end{enumerate}

\section{Face Detection}\label{sec:face_detection}

There are multiple ways to count people in videos.
One way of doing this would be to look for shapes or motion.
Another way would be to detect the faces.
In this thesis we focus on the second option, because persons are more recognizable from their faces then from their body's.
Detecting shapes or body parts, can end up with a database filled with people backs not being of any use.\\

\input{./figures/face_detect_example}
The process of detecting faces is called face detection and for humans is task is quite easy. Explaining this to a computer is a more complicated task.
Face detection can be seen as a specific case of object detection.
In object detection the goal is to find all the locations and sizes of all objects in an image~\cite{wiki:face_detection}.
In face detection we only try to find one object type being the face of a human.
A somewhat desired result is shown in~\ref{fig:face_detect_ex}.
Here we see that the green rectangles show where the faces are in this picture.
Of course one picture can also been seen as an individual frame.
Factors that make it difficult to come up with a general robust solution are:
\begin{itemize}
\item Head rotation and tilt
\item Lightning intensity and angle.
\item Facial expressions.
\item Aging.
\item Image resolution
\item Occluding objects
\end{itemize}

\input{./figures/snap_ex.tex}
To overcome these problems multiple methods are developed of the last years. Each with their own advantages and disadvantages
The most optimal result would hence be, loading a video finding all the faces that are present and saving these faces in a database. Again doing this 100\% accurate is hard, and having no false positives rarely happens.
Once a face detected, the face can be cut from the corresponding frame and resized to a desired output width and height.
Detecting faces is widely used in combination with other techniques. In our project we use face detection in combination with image comparison.
However there are direct applications of face detection.
In photography, we use face detection to focus our lens to this area. This technique is already available on mobile phones. Snapchat~\cite{snap} is another interesting example.
Looking at image~\ref{fig:snap_ex} we see one of Snapchat's many available filters.
Here not only the face is detected but also the angle and then correctly drawing some overlay. This is quite remarkable if we take into account that Snapchat does this in real time on a mobile phone.\\

There are multiple categories for facial detection algorithms. We will list these categories below and discuss relevant papers.

\subsection{Traditional Face Detection}

Traditional face detection algorithms identify facial features by extracting landmarks, or features, from an image.
One of the most used methods is proposed by Voila et al.\cite{viola2004robust}. Here multiple face features are combined to detect a face and thus this papaer falls into this category.
This method was a break though, because it was the first to detect faces in real time.
The algorithm is fast while obtaining high detection rates, it has four main stages.
\begin{enumerate}
\item Haar Feature Selection
\item Creating an Integral Image
\item Adaboost training
\item Cascading Classifiers
\end{enumerate}

\input{./figures/haar_cas_ex.tex}

The haar features are using the fact that all faces share some similar properties.
For example the nose is a bit brighter that the two areas next to it, see~\ref{fig:haar_feature}.
Then the best haar features are combined. By doing this the speed of execution goes up.
These best haar features are applied to all smaller windows of the current frame
The downside of this algorithm is that is struggles with face angles in any direction (one of the problems stated). This is due to the fact that it is trained with frontal faces. An upside is that this method is already available in the OpenCV library~\cite{opencv} with trained cascades.

\subsection{Skin Texture Analysis Face Detection}

DU Cui-huan, et al.\cite{du2013face} uses AdaBoost in combination with a skin model, and hence falls into this category.
This method is applied to videos, and thus enhancing the relevance of the paper to the main research questions.
Two phases are discussed, first the motion area is calculated.
On this area the AdaBoost algorithm from Viola et al.\cite{viola2004robust} is used.
Once this detection is successful clustering is done by a skin model. 
They obtain an accuracy of 92.3\% on a database of 3000 faces.

\subsection{Motion Based Face Detection}

In Paul, et al.\cite{paul2013human} a more general overview is given. Here it is not only focused on the detection of face but also people in general. Also it talks specifically about surveillance videos and hence is relevant.
All methods are grouped in three categories: background subtraction, optical flow and spatio-temporal filters.
These methods are used for detecting motion. Once motion is detected we can verify that it is a human by three major types: shape-based, motion-based and texture based.
In this paper table~\ref{tab:based_methods} is presented.
\input{./tables/human_detection.tex}
However since our research is focused on the human face, this paper and DU Cui-huan, et al.\cite{du2013face} are not relevant to our proposed solution.

\subsection{Deep Learning Face Detection}

An other approach is suggested by Farfade et al.\cite{farfade2015multi}. Here deep learning is used to detect faces and hence it falls into this category. All current benchmarks are done with deep neural nets, or more specifically convolutional neural nets.
In this paper  the problem of multi-view face detection is discussed. While there has been significant research on this
problem, current state-of-the-art approaches for this task
require annotation of facial landmarks.
They also require training dozens of models to fully capture faces in all orientations.
In this paper Deep Dense Face Detector (DDFD) is proposed, a method that does not require pose/landmark annotation and is able to detect faces in a wide range of orientations using a single model based on deep convolutional neural networks.
The proposed method has minimal complexity; unlike other recent deep learning object detection methods, it does not require additional components such as segmentation, bounding-box regression, or SVM classifiers.
Evaluations on popular face detection benchmark datasets show that their single-model face detector algorithm has similar or better performance compared to the previous methods, which are more complex and require annotations of either different poses or facial landmarks.\\

One of the advantages about this paper is that the newest OpenCV~\cite{opencv} comes with functionality for this method.

\subsection{High Tech Camera Face Detection}

High tech cameras have more functionality, for example the camera is able to detect temperature or depth. This extra data could lead to even better face detection methods. We do not have relevant literature in this category. In out problem we do not have these high tech cameras.\\

After this literature research about face detection, we decide to implement both the haar-cascade from Voila et al.\cite{viola2004robust} and the method from Farfade et al.\cite{farfade2015multi}. These methods will be compared. These are both available in the newest version of the OpenCV library and thus we will be using this. Implement one of these methods from scratch would not fit in the time given for a bachelors project.
Once we present our results of the comparison we will choose one of these methods for our final people count method.

\section{Image Comparison}\label{sec:image_comparison}

Now that we discussed how we can detect faces and save these for each frame, we want to have a method for comparing if a face is of the same person or if it is someone different.
Image comparison is a big theme and there are multiple methods discussed in this chapter.
Methods of similarity between two images are useful for the comparison of algorithms devoted to noise reduction, image matching, image coding and restoration. In our thesis it is mainly focused on image matching.
Facial images are a very high-dimensional feature vector. Usually in modern applications some way of reducing this dimensionality is used when working with faces.
Most papers make a comparison between different image comparison techniques.\\

The in Ges\`u et al.\cite{di1999distance} described methods based on distance functions.
Measures of similarity are also used to evaluate lossy compression algorithms and to define pictorial indices in image content based retrieval methods.
In this paper they develop a distance-based approach to image similarity evaluation and they present several image distances which are based on low level features. The sensitivity and effectiveness are tested on real data.
Experimental results indicate better sensitivity of the functions by combining both global intensity and local structural features with respect to conventional intensity-based measures.\\

The next paper describes an empirical comparison Stevens et al.\cite{stevens2000image}.
Using 27 different error functions on a set of 20 example problems.
Each error function measures the similarity between an observed and a predicted image.
The goal of the paper is to select an error best suited to guide a heuristic search algorithm through a space of possible scene configurations.

Another approach is given in Jia et al.\cite{jia2006comparison}. In this paper, three newly proposed histogram-based methods are compared with other three popular methods, including conventional histogram intersection (HI) method, Wong and Cheung's merged palette histogram matching (MPHM) method, and Gevers' colour ratio gradient (CRG) method.
They test their methods on vehicle license plates to classify them.
Experimental results disclose that, the CRG method is the best choice in terms of speed, and the GWHI method can give the best classification results. Overall, the CECH method produces the best performance when both speed and classification performance are concerned.\\

As seen in the above mentioned papers, there are a lot of different methods for image comparison. All papers make a comparison between different methods.
We however will also compare the in OpenCV~\cite{opencv} given histogram comparison methods. These methods are explained in Section~\ref{sec:proposed_solution}.

\section{Face Clustering}\label{sec:face_clustering}

Now that we are able to find the faces from a video, and know what face is the same, we are left with a large database with faces.
To further analyze all these faces, we need somehow a way to simplify the dataset, i.e. reduce it to a small one. This is typically done by putting very similar data items (faces) together, which in turn can be done by what are known as clustering methods.\\

Essentially, any clustering algorithm is an optimization process which receives 2 inputs: a distance function that compares data items, and a desired simplification level (expressed either in the maximal distance that 2 elements in the same cluster can have, or by the max size a cluster can have). Then, it partitions the data items into clusters. Of course, there's a trade-off: if you have fewer clusters, you'll have more items per cluster (more generalization or simplification, but higher error); and if you have more clusters, you'll have fewer data items per cluster (less generalization, but less error). Being able to cluster these faces will increase the functionality of our database.
As an example use, if we want to only look at all the woman we detected.
Then the faces should be clustered on gender.\\

\newpage

In Otto et al.\cite{otto2015efficient} the best practices for efficient clustering of face images are explored.
They apply their clustering on a database of up to one million faces.
This data set is then clustered in a large set of approximately 200 thousand clusters.
The main challenges for clustering face images are:
\begin{itemize}
\item Large dataset size (millions of face images).
\item Large number of classes: a crowd may contain a large number of individuals (tends of thousands, if not more).
\item High intra-class variability and small inter-class sepa-ration: images are captured under unconstrained conditions, with uncooperative subjects, in difficult imaging environments.
\item Unknown number of clusters: the number of individuals present in the collected data is not known a priori, but may contain tens of thousands of clusters.
\item Variable number of samples per cluster: some individuals may be present in only a few images or video frames, others in many.
\end{itemize}
In the conclusion it is stated that, Rank-Ordered clustering is a good trade off between computational power and accuracy.\\

A paper discussing the given example about gender clustering is Orozco et al.\cite{8362114}.
It is stated that the good results obtained using deep convolutional neural networks in vision tasks make them an attractive tool to improve the capacities of gender recognition systems.
They propose a deep convolutional network architecture to classify as male or female person.
Haar features embedded in an AdaBoost are used to obtain candidate regions.
The used data set is the common Labeled Faces in the Wildand Gallagher's dataset.
Their results on the proposed architecture are 95.42\% and 91.48\% accuracy for the training set and for the test set.\\

In Otto et al.\cite{otto2018clustering} a method for clustering a large collection of unlabeled faces is given. They address the problem of clustering faces into an unknown number of identities and thus unknown amount of clusters. 
They state that the problem applies to social media, law enforcement and other applications, where there can be hundreds of millions of faces present.
An approximate Rank-Order clustering is presented that preforms better then other popular clustering algorithms. In their experiments there are up to 123 million faces that are clustered over 10 million clusters. 
Clustering results are analyzed in terms of external (known face labels) and internal (unknown face labels) quality measures, and run-time.
Their algorithm achieves an F-measure of 0.87 on the LFW benchmark (13 K faces of 5,749 individuals), which drops to 0.27 on the largest dataset considered (13 K faces in LFW + 123M distractor images). \\

\newpage

Face clustering is not the best choice for our project. This is because we do not want to lose any identity. This means at most one cluster per person which makes clustering not very useful. Another reason is that face clustering is complex and not doable in the given three months. As last face clustering is not possible in real time and there are not test of images with low resolutions. These reasons lead us to the conclusion that we do not implement or use any of the above given face clustering techniques.

\section{Face Recognition}\label{sec:face_recognition}

We discussed how to identify, compare and cluster faces.
This means only the last step is left undiscussed, being able to recognize people once a face is detected.
A facial recognition system is a technology capable of identifying or verifying a person from a digital image or a video frame from a video source.
There are multiples methods in which facial recognition systems work, but in general, they work by comparing selected facial features from given image with faces within a database.
In this field there has been a lot of recent advances.
As an example usage, the Face ID introduced by Apple makes sure that the owner of the phone can log in using his or her face.
Here first face detection is used to see where in the camera the face is present, hereafter face recognition is used to see that it is indeed the owner of the phone.
To do this securely Apple needs very accurate recognition method, otherwise anyone can log into your phone.
This technology even learns from changes in a user's appearance, and therefore works with hats, scarves, glasses and many sunglasses, beard and makeup.
The advantage that Apple has is that the faces used to log in to a phone are mostly full frontal and of high resolution, or it just does not work.
For our research we want to detect all faces and these faces are of low resolution so counting them complicates the process.\\

A paper discussing the current state-of-the-art is Schroff et al.\cite{schroff2015facenet}.
In this paper a system, called FaceNet, that directly learns a mapping from face images to a compact Euclidean space where distances directly correspond to a measure of face similarity, is presented.
Once this space has been produced, tasks such as face recognition, verification and clustering can be easily implemented using standard techniques with FaceNet embeddings as feature vectors.
Their method uses a deep convolutional network trained to directly optimize the embedding itself, rather than an intermediate bottleneck layer as in previous deep learning approaches.
To train this network, they use triplets of roughly aligned matching / non-matching face patches generated using a novel online triplet mining method.
The benefit of this approach is much greater representational efficiency.
The results are achieved with only 128-bytes per face.
On the widely used Labeled Faces in the Wild (LFW) dataset, the system achieves a new record accuracy of 99.63\%.
On YouTube Faces DB it achieves 95.12\%. 
This system cuts the error rate in comparison to the best published result by 30\% on both datasets.\\

Another widely used method is discussed in Lei et al.\cite{lei2014face}.
In this paper, a method is proposed that is simple and efficient.
It uses face representation feature that adopts the eigenfaces of Local Binary Pattern (LBP) space, referred to as the LBP eigenfaces, for robust face recognition.
In the experiments, the proposed LBP eigenfaces are integrated into two types of classification methods, Nearest Neighbor (NN) and Collaborative Representation-based Classification (CRC).
Experimental results indicate that the classification with the LBP eigenfaces outperforms that with the original eigenfaces and LBP histogram.\\

Hmani et al.\cite{8364450} is interested in the reproducibility of face recognition systems.
By reproducibility they mean: is the scientific community, and are the researchers from different sides, capable of reproducing the last published results by a big company, that has at its disposal huge computational power and huge proprietary databases?
With the constant advancements in GPU computation power and availability of open-source software, the reproducibility of published results should not be a problem.
But, if architectures of the systems are private and databases are proprietary, the reproducibility of published results can not be easily attained.
To tackle this problem, they focus on training and evaluation of face recognition systems on publicly available data and software.
Also they are interested in comparing the best Deep Neural Net (DNN) based results with a baseline ``classical'' system.
This paper exploits the OpenFace open-source system to generate a deep convolutional neural network model using publicly available datasets.
It is studied what the impact of the size of the datasets, their quality is and they compare the performance to a classical face recognition approach.
The focus is to have a fully reproducible model.
To this end, they used publicly available datasets (FRGC, MS-celeb-1M, MOBIO, LFW), as well publicly available software (OpenFace) to train our model in order to do face recognition. Our best trained model achieves 97.52\% accuracy on the Labelled in the Wild dataset (LFW) dataset which is lower than Google's best reported results of 99.96\% but
slightly better than FaceBook's reported result of 97.35\%.\\

This is a quite interesting point, since this means that it should be possible for researchers to also reproduce these results.
However face recognition does not work for face images with poor resolution and they requires the face in standard position, light and distance. There are methods for face recognition using face images with poor resolution~\cite{karaaba2016face}, but understanding and implementing  these methods are not doable for the given three months of the bachelor project and the risk of not-success is high, that is the reason why we use histograms.

\section{Summary}

Below the table~\ref{tab:sum_tech} summarizing all relevant literature discussed in this chapter, is summarized.
We consider the following four criteria for evaluating the methods in the papers, validation, availability, complexity and usability.
The reason for this is that we want next to pick an ``optimal'' method to build upon.
Optimal here means that it satisfies as much criteria as possible.
A more detailed description of what the criteria are is given below.

\begin{itemize}
\item Validation: How well the algorithm is tested by the authors in the paper, and how promising the results are looking
for this project;
\item Availability: Whether the source code is available on the Internet, or if not, whether the algorithm is described in
enough detail for us to reproduce it;
\item Complexity: Whether the algorithm could be executed fast enough to be close to real time;
\item Usability: Refers to the number of parameters and their meaningfulness. A large number of parameters is bad for
ease of use, and it should be easily understandable what a parameter affects and what the result of adjusting it will
be.
\end{itemize}

\input{./tables/relevant_literature_sum.tex}

\section{Conclusion Of The Relevant Work}

As stated in the relevant sections, we will compare two different face detection algorithms. One being the haar-cascade the other one being a deep neural net. Our image comparison will be based on the predefined methods of histogram comparison. In our final result we will combine image comparison with face detection to reduce the amount of faces as much as possible without deleting persons.
