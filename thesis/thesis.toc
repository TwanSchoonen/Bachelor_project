\babel@toc {english}{}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {chapter}{\numberline {2}Related Work}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Face Detection}{7}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Traditional Face Detection}{8}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Skin Texture Analysis Face Detection}{8}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Motion Based Face Detection}{9}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Deep Learning Face Detection}{9}{subsection.2.1.4}
\contentsline {subsection}{\numberline {2.1.5}High Tech Camera Face Detection}{10}{subsection.2.1.5}
\contentsline {section}{\numberline {2.2}Image Comparison}{10}{section.2.2}
\contentsline {section}{\numberline {2.3}Face Clustering}{11}{section.2.3}
\contentsline {section}{\numberline {2.4}Face Recognition}{13}{section.2.4}
\contentsline {section}{\numberline {2.5}Summary}{14}{section.2.5}
\contentsline {section}{\numberline {2.6}Conclusion Of The Relevant Work}{15}{section.2.6}
\contentsline {chapter}{\numberline {3}Proposed Solution}{16}{chapter.3}
\contentsline {section}{\numberline {3.1}Detect Faces}{17}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Haar Cascade}{18}{subsection.3.1.1}
\contentsline {subsection}{\numberline {3.1.2}Deep Neural Net}{19}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Compute And Compare Histograms}{19}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Correlation}{20}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}Chi-Square}{20}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Intersection}{20}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Bhattacharyya distance}{21}{subsection.3.2.4}
\contentsline {section}{\numberline {3.3}Overview}{21}{section.3.3}
\contentsline {chapter}{\numberline {4}Results}{23}{chapter.4}
\contentsline {section}{\numberline {4.1}Face Detection}{23}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Table Overview}{23}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Individual Frames}{25}{subsection.4.1.2}
\contentsline {subsubsection}{Isis Video 4}{25}{section*.14}
\contentsline {subsubsection}{Isis Video 11}{26}{section*.19}
\contentsline {subsubsection}{Isis Video 5}{28}{section*.24}
\contentsline {subsubsection}{Random Video 1}{30}{section*.29}
\contentsline {section}{\numberline {4.2}Image Comparison}{32}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Table Overview}{32}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Image Examples}{33}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Histogram Plots}{36}{subsection.4.2.3}
\contentsline {subsubsection}{Correlation}{36}{section*.40}
\contentsline {subsubsection}{Chi-Square}{37}{section*.42}
\contentsline {subsubsection}{Intersection}{37}{section*.44}
\contentsline {subsubsection}{Bhattacharyya Bistance}{38}{section*.46}
\contentsline {section}{\numberline {4.3}People Count}{39}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Different Histogram Methods}{39}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}Multiple Detection Methods}{40}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}Variant Threshold Levels}{41}{subsection.4.3.3}
\contentsline {subsection}{\numberline {4.3.4}Final Results}{42}{subsection.4.3.4}
\contentsline {subsection}{\numberline {4.3.5}Program Output}{44}{subsection.4.3.5}
\contentsline {chapter}{\numberline {5}Conclusion}{45}{chapter.5}
\contentsline {section}{\numberline {5.1}Limitations}{46}{section.5.1}
\contentsline {section}{\numberline {5.2}Stimulus For Future Work}{47}{section.5.2}
\contentsline {chapter}{Bibliography}{47}{section.5.2}
\contentsline {chapter}{Appendices}{50}{section*.54}
\contentsline {chapter}{\numberline {A}Software Documentation}{51}{Appendix.1.A}
\contentsline {section}{\numberline {A.1}Technology stack}{51}{section.1.A.1}
\contentsline {section}{\numberline {A.2}Requirements}{51}{section.1.A.2}
\contentsline {section}{\numberline {A.3}Build instructions and user interaction}{53}{section.1.A.3}
\contentsline {section}{\numberline {A.4}Software design}{54}{section.1.A.4}
