\beamer@sectionintoc {1}{Introduction}{3}{0}{1}
\beamer@sectionintoc {2}{Related Work}{6}{0}{2}
\beamer@sectionintoc {3}{Proposed Solution}{8}{0}{3}
\beamer@sectionintoc {4}{Data}{15}{0}{4}
\beamer@sectionintoc {5}{Results}{16}{0}{5}
\beamer@subsectionintoc {5}{1}{Face Detection}{16}{0}{5}
\beamer@subsectionintoc {5}{2}{Image Comparison}{21}{0}{5}
\beamer@subsectionintoc {5}{3}{People Count}{23}{0}{5}
\beamer@subsectionintoc {5}{4}{Software Output}{26}{0}{5}
\beamer@sectionintoc {6}{Conclusion}{88}{0}{6}
